FROM node:carbon

# Install software 
RUN apt-get install -y git
RUN npm install forever -g
VOLUME /deribot

RUN git clone https://bitbucket.org/alari777/deribot /deribot/

# Create app directory
WORKDIR /deribot

EXPOSE 80
CMD git pull origin master && npm install -y && npm start