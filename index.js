require('dotenv').config();
const TelegramBot = require('node-telegram-bot-api');
const Mysql = require('./mysql');

const bot = new TelegramBot(process.env.TELEGRAMM_CONNECT, {polling: true});

const { TelegramClient } = require('messaging-api-telegram');
const clientTeleg = TelegramClient.connect(process.env.TELEGRAMM_CONNECT_NOTS);

const Menu = require('./menu');

const menu = new Menu();

const nodemailer = require('nodemailer');

const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: process.env.E_MAIL_NAME,
        pass: process.env.E_MAIL_PASSWORD
    }
});

const validator = require('validator');

bot.onText(/\/(start)|(menu)|(getnots)/, async (msg) => {
    if (!msg.from.is_bot) {
        console.log(msg)
        const foo = {
            id: msg.from.id,
            firstName: msg.from.first_name,
            isBot: msg.from.is_bot,
            languageCode: msg.from.language_code,
        };

        let title = '📗 <b>To Get Notifications.</b>\n\n';
        title += 'For this one you should ENTER your email in our system.\n\n';
        title += 'After this you could get next notifications:\n';
        title += '1. <b>Stoploss</b>\n\n';
        title += '<b>OK. Please enter your email in our system and press ENTER:</b>';
        bot.sendMessage(msg.chat.id, title, menu.fullmenu.get('main-menu'), {
            reply_markup: {
                force_reply: true,
            },
        });
    }
});

bot.onText(/\/(resetemail)|(cancelnots)/, async (msg) => {

});

bot.on('message', (msg) => {
    const chatId = msg.chat.id;

    console.log('CHAT ID:', msg);
    console.log('Who sent text:', msg.from.id);
    console.log('Sending text:', msg.text);

    const htmlBody = `<h1>Hello,</h1><p>Follow for next link:</p><a href='http://deribot.info/member/nots.php?id=${msg.from.id}' target='_blank'>Get Nots</a></a>`;

    if (validator.isEmail(msg.text)) {
        const mailOptions = {
            from: process.env.E_MAIL_NAME,
            to: msg.text,
            subject: 'Subs to deribit nots',
            html: htmlBody
        };

        let msgText = 'Notifications by telegramm\n\n';
        msgText += `✅ ID: ${msg.from.id}\n`;
        msgText += `✅ Email: ${msg.text}\n`;

        clientTeleg.sendMessage(142545448, msgText, {
            disable_web_page_preview: true,
            disable_notification: true,
        });

        clientTeleg.sendMessage(159856577, msgText, {
            disable_web_page_preview: true,
            disable_notification: true,
        });

        let title = '<b>OK! Notifications.</b>\n\n';
        title += '✅ Notifications will be activated  soon.\n';

        bot.sendMessage(msg.chat.id, title, menu.fullmenu.get('main-menu'), {
            reply_markup: {
                force_reply: true,
            },
        });
        /*
        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                console.log(error);
            } else {
                console.log('Email sent: ' + info.response);
                let title = '<b>OK! The email was sent.</b>\n\n';
                title += '🔔 Please check your mailbox. Also please check spam folder.\n';
                title += '📗 Follow a very little instruction guide in this email.\n';

                bot.sendMessage(msg.chat.id, title, menu.fullmenu.get('main-menu'), {
                    reply_markup: {
                        force_reply: true,
                    },
                });
            }
        });
        */
    }
});

bot.on('callback_query', async (msg) => {
    if (!msg.from.is_bot) {
        const foo = {
            id: msg.from.id,
            firstName: msg.from.first_name,
            isBot: msg.from.is_bot,
            languageCode: msg.from.language_code,
        };

        try {
            switch (msg.data) {
                case 'enter-email': {
                    console.log('MSG:', 'enter-email');
                    break;
                }

                case 'enter-login': {
                    console.log('MSG:', 'enter-login');
                    break;
                }
            }
        } catch (e) {
            console.log(e);
        }
    }
});