class Menu {
  constructor() {
    this.fullmenu = new Map();

    this.createFullMenu();
    return this;
  }

  createFullMenu() {
    let arr1 = [];
    let arr2 = [];

    // arr1 = ['Enter email', 'Enter login'];
    // arr2 = ['enter-email', 'enter-login'];
    this.fullmenu.set('main-menu', Menu.createSubMenu(arr1, arr2));

    return null;
  }

  static createSubMenu(text, callbacks) {
    const inlineKeyboardArr = [];
    text.forEach((dataset, index) => {
      const arr = [];
      const foo = {};
      foo.text = dataset;
      foo.callback_data = callbacks[index];
      arr.push(foo);
      inlineKeyboardArr.push(arr);
    });

    return {
      parse_mode: 'html',
      disable_web_page_preview: true,
      reply_markup: JSON.stringify({
        inline_keyboard: inlineKeyboardArr,
      }),
    };
  }
}

module.exports = Menu;