require('dotenv').config();
const mysql = require('mysql');

class Mysql {
    constructor() {
        this.connection = mysql.createConnection({
            host: process.env.D_MYSQL_HOST,
            user: process.env.D_MYSQL_USERNAME,
            password: process.env.D_MYSQL_PASSWORD,
            database: process.env.D_MYSQL_DB
        });

        return this.connection.connect();
    }

    async getIpContainer() {
        return new Promise((resolve, reject) => {
            this.connection.query({
                sql: 'SELECT ' +
                    '`con`.`cnt_ip`' +
                    'FROM ' +
                    '`god_db9`.`containers` AS `con`' +
                    'WHERE (' +
                    '`con`.`cnt_id_user` = ? ' +
                    ') LIMIT 1; ',
                timeout: 40000, // 40s
                values: [process.env.D_USER_ID]
            }, (error, results, fields) => {
                if (error) {
                    console.error(error);
                    reject(error);
                }
                // console.log(results[0].cnt_ip);
                resolve(results[0].cnt_ip);
            });
        });
    }

    async getUserId(email) {
        return new Promise((resolve, reject) => {
            this.connection.query({
                sql: 'SELECT ' +
                    '`us`.`id`' +
                    'FROM ' +
                    '`god_db9`.`users` AS `us`' +
                    'WHERE (' +
                    '`us`.`usr_login` = ? ' +
                    ') LIMIT 1; ',
                timeout: 40000, // 40s
                values: [email]
            }, (error, results, fields) => {
                if (error) {
                    console.error(error);
                    reject(error);
                }
                // console.log(results[0].cnt_ip);
                if (typeof results[0].id !== 'undefined') {
                    resolve(results[0].id);
                } else {
                    resolve(null);
                }
            });
        });
    }

    async setUpdateContainer() {
        return new Promise((resolve, reject) => {
            this.connection.query({
                sql: 'UPDATE ' +
                    '`god_db9`.`actions`' +
                    'SET ' +
                    '`actions`.`act_docker_update` = ? ' +
                    'WHERE (' +
                    '`actions`.`act_id_user` = ?' +
                    ') LIMIT 1; '
                ,
                timeout: 40000,
                values: [0, process.env.D_USER_ID]
            }, (error, results, fields) => {
                if (error) {
                    console.error(error);
                    reject(error);
                }
                resolve(results);
            });
        });
    }


    async doneMysql() {
        return new Promise((resolve, reject) => {
            this.connection.end((err) => {
                if (err) {
                    console.error(err);
                    reject(err);
                }
                resolve(null);
            });
        });
    }
}

module.exports = Mysql;